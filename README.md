#### Учасники проекту:
- Амір Джагатспанян (виконував завдання для студента №1)
- Шахаєв Сергій (виконував завдання для студента №2)

#### Завдання для студента №1
-Зверстати шапку сайту з верхнім меню та   секцію People Are Talking About Fork.

#### Завдання для студента №2
-Зверстати блок Revolutionary Editor, секцію Here is what you get, секцію Fork Subscription Pricing.


#### Проект створювався за допомогою Gulp та NPM пакетів. Без сторонніх бібліотек.

#### перелік пакетів:
- browser-sync
- gulp
- gulp-autoprefixer
- gulp-clean
- gulp-clean-css
- gulp-concat
- gulp-imagemin
- gulp-js-minify
- gulp-sass
- gulp-uglify
- sass

#### Основні завдання gulp
- build -  очищення "dist", компіляція scss в css, конкатенація та мініфікованість сss та  js, оптимізація картинок  
- dev -  запук сервера та слідкування за змінами. 
- default -  виконує завдання build, а потім dev

#### Посилання на сайт 

https://step-project-forkio-shakhaiev-sv-4d78edf88f45d64bfaefb962f7422d.gitlab.io/