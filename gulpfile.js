const gulp = require('gulp');
const clean = require('gulp-clean');
const concat = require('gulp-concat');

//css/sass
const scss = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');

//js
const uglify = require('gulp-uglify');

//img
const imgmin = require('gulp-imagemin');

//server
const browserSync = require('browser-sync');


const paths = {
    styles: {
        src: "./src/styles/**/*.scss",
        dest: "./dist/style"
    },
    scripts: {
        src: "./src/scripts/**/*.js",
        dest: "./dist/script"
    },
    images: {
        src: "./src/images/**/*.{png,jpg,jpeg,gif,webp,svg}",
        dest: "./dist/images"
    }
}

//clean paths
gulp.task('clean', function (done) {
    gulp
        .src('./dist', { allowEmpty: true })
        .pipe(clean())
        .on('finish', done)
});

//css

gulp.task('styles', function (done) {
    gulp
        .src(paths.styles.src)
        .pipe(scss().on('error', scss.logError))
        .pipe(concat('style.min.css'))
        .pipe(autoprefixer({
            cascade: false,
        }))
        .pipe(cleanCSS({
            level: 2
        }))
        .pipe(gulp.dest(paths.styles.dest))
        .on('end', done)
})

//js
gulp.task('scripts', function (done) {
    gulp
        .src(paths.scripts.src)
        .pipe(concat('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(paths.scripts.dest))
        .on('end', done)
})

// img
gulp.task('imgmin', function (done) {
    gulp
        .src(paths.images.src)
        .pipe(imgmin())
        .pipe(gulp.dest(paths.images.dest))
        .on('end', done);
})

//server

gulp.task('server', function (done) {
    browserSync.init({
        server: {
            baseDir: './'
        },
        open: true
    })

    gulp.watch('*.html').on('change', browserSync.reload);
    gulp.watch(paths.styles.src, gulp.series('styles')).on('change', browserSync.reload);
    gulp.watch(paths.scripts.src, gulp.series('scripts')).on('change', browserSync.reload);
    gulp.watch(paths.images.src, gulp.series('imgmin')).on('change', browserSync.reload);
})


gulp.task('build', gulp.series('clean', gulp.parallel('styles', 'scripts', 'imgmin')))
gulp.task('dev', gulp.series('server'))

gulp.task('default', gulp.series('build', 'server'))





